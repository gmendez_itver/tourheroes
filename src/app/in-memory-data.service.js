"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var InMemoryDataService = /** @class */ (function () {
    function InMemoryDataService() {
    }
    InMemoryDataService.prototype.createDb = function () {
        var heroes = [
            { id: 11, name: 'Kaliman 2' },
            { id: 12, name: 'Dragon Ball' },
            { id: 13, name: 'El Santo' },
            { id: 14, name: 'Blue Daemon' },
            { id: 15, name: 'El Chapulin Colorado' },
            { id: 16, name: 'Memin Pinguin' },
            { id: 17, name: 'Condorito' },
            { id: 18, name: 'Pikachu' },
            { id: 19, name: 'Los hermanos Aldama' },
            { id: 20, name: 'Mama Lucha' }
        ];
        return { heroes: heroes };
    };
    return InMemoryDataService;
}());
exports.InMemoryDataService = InMemoryDataService;
//# sourceMappingURL=in-memory-data.service.js.map